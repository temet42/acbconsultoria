<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>A.C.B Consultoria</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/icon.png">
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome CSS
		============================================ -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- et-line-fonts CSS
		============================================ -->
        <link rel="stylesheet" href="css/et-line-fonts.css">
		<!-- ionicons CSS
		============================================ -->
        <link rel="stylesheet" href="css/ionicons.min.css">
		<!-- magnific-popup CSS
		============================================ -->
        <link rel="stylesheet" href="css/magnific-popup.css">
		<!-- animate-headline css
		============================================ -->
        <link rel="stylesheet" href="css/animate-headline.css">
        <!-- venobox CSS
		============================================ -->
        <link rel="stylesheet" href="css/venobox.css">
		<!-- slick CSS
		============================================ -->
        <link rel="stylesheet" href="css/slick.css">
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/owl.transitions.css">
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="css/animate.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="style.css">
		<!-- responsive CSS
		============================================ -->
        <link rel="stylesheet" href="css/responsive.css">
        
        <!-- responsive CSS
		============================================ -->
        <link rel="stylesheet" href="css/modal.css">

		<!-- modernizr JS
		============================================ -->		
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!-- Add your site or application content here -->
        <div class="wrapper">
            <header class="header-area">
                <!-- Menu Area
                ============================================ -->
                <div id="main-menu" class="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="logo float-left navbar-header">
                                    <a class="navbar-brand" href="#"><img src="" alt="LOGO"></a>
                                    <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu-2">
                                        <i class="fa fa-bars menu-open"></i>
                                        <i class="fa fa-times menu-close"></i>
                                    </button>
                                </div>
                                <div class="main-menu float-right collapse navbar-collapse" id="main-menu-2">
                                    <nav>
                                        <ul class="menu one-page">
                                            <li class="active"><a href="#home-area">HOME</a></li>
                                            <li><a href="#about-area">Sobre   </a></li>
                                            <li><a href="#features-area">Serviços</a></li>
                                            <li><a href="#loremipsum">Lorem Ipsum</a></li>           
                                            <li><a href="#review-area">Lorem Ipsum</a></li>
                                            <li><a href="#parceiro">Seja Parceiro </a></li>
                                            <li><a href="#pricing-area">Clientes </a></li>
                                            <li><a href="#support-area">Contato</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- sliders area
            ============================================ -->
            <div id="home-area">
                <div class="slider-active">
                    <div class="sliders-responsive main-slider-area bg-oapcity-40 slider-active-fix" style="background-image: url(img/bg-img/bg-1.jpg)">
                        <div class="container">
                            <div class="row">
                                <div class="home-sliders clearfix mid-mrg">
                                    <div class="col-md-7 col-sm-8">
                                        <div class="top-text pt-120 mid-mrg">
                                            <div class="slider-text">
                                                <h2>Lorem ipsum</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor. </p>
                                        <!--  <div class="button-set">
                                                    <a class="button" href="#">
                                                        DOWNLOAD
                                                    </a>
                                                    <a class="button active" href="#">
                                                        learn more
                                                    </a>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-4">
                                        <div class="slider-imgj mid-mrg">
                                        <img src="img/mobile/1.png" alt="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sliders-responsive main-slider-area bg-oapcity-40 slider-active-fix" style="background-image: url(img/bg-img/bg-4.jpg)">
                        <div class="container">
                            <div class="row">
                                <div class="home-sliders clearfix mid-mrg">
                                    <div class="col-md-7 col-sm-8">
                                        <div class="top-text pt-120 mid-mrg">
                                            <div class="slider-text">
                                                <h2>Lorem Ipsum</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor. </p>
                                               <!-- <div class="button-set">
                                                    <a class="button" href="#">
                                                        DOWNLOAD
                                                    </a>
                                                    <a class="button active" href="#">
                                                        learn more
                                                    </a>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-4">
                                        <div class="slider-imgj mid-mrg">
                                            <img src="img/mobile/1.png" alt="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sliders-responsive main-slider-area bg-oapcity-40 slider-active-fix" style="background-image: url(img/bg-img/bg-1.jpg)">
                        <div class="container">
                            <div class="row">
                                <div class="home-sliders clearfix mid-mrg">
                                    <div class="col-md-7 col-sm-8">
                                        <div class="top-text pt-120 mid-mrg">
                                            <div class="slider-text">
                                                <h2>Lorem Ipsum</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.</p>
                                                <!--<div class="button-set">
                                                    <a class="button" href="#">
                                                        DOWNLOAD
                                                    </a>
                                                    <a class="button active" href="#">
                                                        learn more
                                                    </a>
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-4">
                                        <div class="slider-imgj mid-mrg">
                                            <img src="img/mobile/1.png" alt="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- about area
            ============================================ -->
            <div id="about-area" id="features-area" class="all-about gray-bg ptb-120" >
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="video-img-icon">
                                <div class="about-bottom-img">
                                    <img src="img/blog/1.jpg" alt="">
                                </div>
                            </div>
                        </div>	
						<div class="col-md-6 col-sm-12">
							<div class="about-bottom-left clearfix">
								<h2>Lorem Ipsum</h2>
								<p class="about-pb">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor. Vivamus dolor magna, pretium id congue quis, ornare quis massa. Aenean accumsan sapien id hendrerit mollis. Ut egestas egestas eros, a placerat neque eleifend sit amet. Fusce in lobortis neque. Aliquam et tempus lacus. Suspendisse erat magna, malesuada pulvinar sapien ac, gravida suscipit lorem. Nam vel mollis urna. Quisque et justo sed purus mattis pellentesque. Morbi id urna suscipit, feugiat sem vel, euismod nisl. Donec euismod enim vitae pellentesque consequat. Nulla lacinia tempor sapien sed consequat. Nulla molestie fringilla mi, vel placerat nunc lobortis nec. </p>	
							</div>
						</div>
                    </div>
                </div>
            </div>

<!-- service area
            ============================================ -->
            <div  id="features-area">
                <div class="ptb-120">
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-sm-6 col-md-3"> 
                                <div class="single-item res-sm text-center">
                                    <div class="single-item-icon">
                                        <i class="icon-puzzle"></i>
                                    </div>
                                    <h3 class="ht-pt">Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="single-item res-sm res-xs text-center">
                                    <div class="single-item-icon">
                                        <i class="icon-heart"></i>
                                    </div>
                                    <h3 class="ht-pt">Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="single-item text-center res">
                                    <div class="single-item-icon">
                                        <i class="icon-megaphone"></i>
                                    </div>
                                    <h3 class="ht-pt">Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <div class="single-item text-center">
                                    <div class="single-item-icon">
                                        <i class="icon-genius"></i>
                                    </div>
                                    <h3 class="ht-pt">Lorem Ipsum</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- features area -->
        <!--<div id="features-area" class="ptb-120 fix">
                <div class="container">
                    <div class="about-bottom-left blog-mrg clearfix text-center">
                        <h2>Awesome features</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have <br>suffered alteration in some form, </p>
                    </div>
					<div class="row">
						<div class="col-lg-4 col-md-4 featured-left pt-50 pr-0 xs-res">
							<div class="single-features-list text-right">
								<div class="feature-list-icon">
									<i class="fa fa-heart" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>Great Support</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. 
									</p>
								</div>
							</div>
							<div class="single-features-list text-right">
								<div class="feature-list-icon">
									<i class="fa fa-language" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>Clean Code</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. 
									</p>
								</div>
							</div>
							<div class="single-features-list text-right">
								<div class="feature-list-icon">
									<i class="fa fa-desktop" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>Unique Design</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa.  
									</p>
								</div>
							</div>						
						</div>
						<div class="col-lg-4 col-md-4">
							<div class="feature-img text-center">
								<img src="img/mobile/1.png" alt="" />
							</div>
						</div> 
						<div class="col-lg-4 col-md-4 featured-right pt-50 pl-0">
							<div class="single-features-list text-left">
								<div class="feature-list-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>Unlimited Features</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa.
									</p>
								</div>
							</div>
							<div class="single-features-list text-left">
								<div class="feature-list-icon">
									<i class="fa fa-sun-o" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>High Resolution</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa. 
									</p>
								</div>
							</div>
							<div class="single-features-list text-left res-features">
								<div class="feature-list-icon">
									<i class="fa fa-paint-brush" aria-hidden="true"></i>
								</div>
								<div class="feature-list-text">
									<h3>Modern Design</h3>
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel volutpat felis, eu condimentum massa.  
									</p>
								</div>
							</div>						
						</div>
					</div>
                </div>
            </div>-->
            <!-- Funfact Area Start -->
        <!-- <section class="funfact-area clearfix gray-bg pt-100 pb-70" id="loremipsum">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 text-center">
                            <div class="funfact-single mb-30">
                                <div class="funfact-img">
                                    <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                </div>
                                <div class="funfact-count text-uppercase">
                                    <h3><span class="counter">2000</span></h3>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 text-center">
                            <div class="funfact-single mb-30">
                                <div class="funfact-img">
                                    <i class="fa fa-female" aria-hidden="true"></i>
                                </div>
                                <div class="funfact-count text-uppercase">
                                    <h3><span class="counter">1000</span></h3>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3  col-sm-6 text-center">
                            <div class="funfact-single mb-30">
                                <div class="funfact-img">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="funfact-count text-uppercase">
                                    <h3><span class="counter">5000</span></h3>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 text-center">
                            <div class="funfact-single mb-30">
                                <div class="funfact-img">
                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                </div>
                                <div class="funfact-count text-uppercase">
                                    <h3><span class="counter">5000</span></h3>
                                    <h5>Lorem Ipsum</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
            <!-- screenshort area -->
            <div id="loremipsum" class="screenshot-area gray-bg  ptb-120">
                <div class="col-md-6 col-sm-6 p-0 col-md-push-6 col-sm-push-6">
                    <div class="screenshot-slider">
                        <div class="single-screenshot">
                            <div class="image">
                                <a class="venobox" href="img/screenshort/1.jpg" ><img src="img/screenshort/1.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="single-screenshot">
                            <div class="image">
                                <a class="venobox" href="img/screenshort/2.jpg" ><img src="img/screenshort/2.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="single-screenshot">
                            <div class="image">
                                <a class="venobox" href="img/screenshort/1.jpg" ><img src="img/screenshort/3.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="single-screenshot">
                            <div class="image">
                                <a class="venobox" href="img/screenshort/2.jpg" ><img src="img/screenshort/4.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="single-screenshot">
                            <div class="image">
                                <a class="venobox" href="img/screenshort/1.jpg" ><img src="img/screenshort/1.jpg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="screenshot-text">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-6 pr-0 awesome-desc">
                                <div class="about-bottom-left">
                                    <h2>Lorem ipsum</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                                </div>
                            </div>
                        </div>
                    </div>			
                </div>
            </div>
            <!-- download Area
            ============================================ -->
            <div class="download-area bg-2 bg-oapcity-40 pt-100 pb-120" >
                <div class="container">
                    <div class="about-bottom-left download-mrg clearfix text-center">
                        <h2>Lorem ipsum</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                    </div>
                <!--<div class="row">
                        <div class="col-md-12 text-center">
                            <div class="download-button-set">
                                <a class="download active" href="#">
                                    <i class="fa fa-apple" aria-hidden="true"></i>
                                    <span>
                                        Available on the
                                        <span class="large-text-2">App Store</span>
                                    </span>
                                </a>
                                <a class="download btn-mobile" href="#">
                                    <i class="fa fa-android" aria-hidden="true"></i>
                                    <span>
                                        Available on the
                                        <span class="large-text-2">Play Store</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
            <!-- pricing area
            ============================================ -->
     <!--       <div id="pricing-area" class="pricing-area ptb-120">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="about-bottom-left pricing-mrg clearfix">
								<h2>Pricing Plan</h2>
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have <br>suffered alteration in some form, binjected humour passages of Lorem.</p>
							</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-price-package">
                                <div class="price-title">
                                    <h3>BASIC</h3>
                                </div>
                                <div class="price">
                                    <h4>
                                        <span class="text-large">Free</span>
                                    </h4>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>10 Emails</li>
                                        <li>100 GB Disk</li>
                                        <li>Unlimited Bandwidth</li>
                                        <li>Free Support</li>
                                    </ul>
                                    <div class="price-btn">
                                        <button class="button" type="button">GET STARTED</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-price-package active res-xs">
                                <div class="price-title">
                                    <h3>ADVANCED</h3>
                                </div>
                                <div class="price">
                                    <h4>
                                        <span class="text-top">$</span>
                                        <span class="text-large">12</span>
                                    </h4>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>10 Emails</li>
                                        <li>100 GB Disk</li>
                                        <li>Unlimited Bandwidth</li>
                                        <li>Free Support</li>
                                    </ul>
                                    <div class="price-btn">
                                        <button class="button active" type="button">GET STARTED</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="single-price-package">
                                <div class="price-title">
                                    <h3>PROFESSIONAL</h3>
                                </div>
                                <div class="price">
                                    <h4>
                                        <span class="text-top">$</span>
                                        <span class="text-large">22</span>
                                    </h4>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>10 Emails</li>
                                        <li>100 GB Disk</li>
                                        <li>Unlimited Bandwidth</li>
                                        <li>Free Support</li>
                                    </ul>
                                    <div class="price-btn">
                                        <button class="button" type="button">GET STARTED</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
           
            <!-- testimonials Area
            ============================================ -->
            <div id="review-area" class="ptb-120 pb-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="slider-2">
                                <div class="single-team text-center">
                                    <div class="team-image">
                                        <img src="img/testimonial/1.png" alt="">
                                    </div>
                                    <div class="team-details mt-20">
                                        <h2>Lorem ipsum</h2>
                                        <div class="reating-icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="team-social fix">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                                        </div>
                                        <div class="test-icon">
                                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-team text-center">
                                    <div class="team-image">
                                        <img src="img/testimonial/1.png" alt="">
                                    </div>
                                    <div class="team-details mt-20">
                                        <h2>Lorem ipsum</h2>
                                        <div class="reating-icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="team-social fix">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                                        </div>
                                        <div class="test-icon">
                                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="single-team text-center">
                                    <div class="team-image">
                                        <img src="img/testimonial/1.png" alt="">
                                    </div>
                                    <div class="team-details mt-20">
                                        <h2>Lorem ipsum</h2>
                                        <div class="reating-icon">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="team-social fix">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                                        </div>
                                        <div class="test-icon">
                                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <!-- newsletter area
            ============================================ -->
            <div class="newsletter-area ptb-120 bg-img-4" id="parceiro">
                <div class="container">
                    <div class="about-bottom-left download-mrg clearfix text-center">
                        <h2>Lorem ipsum</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et lorem nec arcu lacinia suscipit quis vitae tortor.  <br>Vivamus dolor magna, pretium id congue quis, ornare quis massa. </p>
                    </div>
                   <div class="row">
                        <div class="subscribe-form text-center col-xs-12 col-md-offset-4 col-md-4 col-sm-8 col-sm-offset-2">
                        <!--<form id="mc-form" class="mc-form">
                                <input id="mc-email" type="email" autocomplete="off" placeholder="Enter your Email address">
                                <button  id="mc-submit" type="submit"><i class="ion-android-send"></i></button>
                            </form>
                            <!-- mailchimp-alerts Start 
                            <div class="mailchimp-alerts text-centre">
                                 <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end 
                                 <div class="mailchimp-success"></div><!-- mailchimp-success end 
                                 <div class="mailchimp-error"></div><!-- mailchimp-error end 
                            </div><!-- mailchimp-alerts end-->
                            <div class="btn btn-primary btn-lg"  data-toggle="modal" data-target="#squarespaceModal">SEJA PARCEIRO</div>
                        </div>
                    </div>
                </div>
            </div>

        <!--MODAL-->
        <!-- line modal -->
        <div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title text-center" id="lineModalLabel">SEJA PARCEIRO</h3>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form>
                    <div class="form-group col-md-12">
                        <label for="exampleInputname">Nome</label>
                        <input type="name" class="form-control" id="exampleInputName" placeholder="Nome">
                    </div>
                    <div class="form-group col-md-12" >
                        <label for="exampleInputEmail">Email </label>
                        <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email">
                    </div>
                    <div class="form-group col-md-12 ">
                        <label for="exampleInputTelefone">Telefone</label>
                        <input type="text" class="form-control" id="exampleInputTelefone" placeholder="Telefone">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputCep">CEP</label>
                        <input type="text" class="form-control" id="exampleInputCep" placeholder="CEP">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputCep">Número</label>
                        <input type="text" class="form-control" id="exampleInputNumero" placeholder="Número">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputCep1">Assunto</label>
                        <input type="text" class="form-control" id="exampleInputAssunto" placeholder="Assunto">
                    </div>
                    
                    <div class="form-group col-md-12">
                        <label for="exampleFormControlTextarea">Mensagem</label>
                        <textarea class="form-control" id="exampleFormControlTextarea" rows="7"></textarea>
                    </div>
                    </form>
                </div>
                <div class="modal-footer form-group " style="margin-left:5%; margin-right:5%;">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">
                        <button class="submit" type="submit">ENVIAR</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <!--/MODAL-->

            
            <!--<div class="blog-area gray-bg pt-100 pb-120">
                <div class="container">
                    <div class="about-bottom-left blog-mrg clearfix text-center">
                        <h2>Our Blog</h2>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have <br>suffered alteration in some form, </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="blog-img-text res">
                                <div class="blog-img">
                                    <a href="#"><img src="img/blog/1.jpg" alt=""></a>
                                </div>
                                <div class="blog-text">
                                    <span>15 May 2016</span>
                                    <h4><a href="#">There are many variations of passa Lorem.</a></h4>
                                    <p>Sed ut perspiciatis unde omnis natus error sit voluptatem accusan dolorentium, totam bondho.</p>
                                    <a href="#">Read More <i class="ion-arrow-right-c"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="blog-img-text">
                                <div class="blog-img">
                                    <a href="#"><img src="img/blog/2.jpg" alt=""></a>
                                </div>
                                <div class="blog-text">
                                    <span>15 May 2016</span>
                                    <h4><a href="#">There are many variations of passa Lorem.</a></h4>
                                    <p>Sed ut perspiciatis unde omnis natus error sit voluptatem accusan dolorentium, totam bondho.</p>
                                    <a href="#">Read More <i class="ion-arrow-right-c"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- brand logo
            ============================================ -->
            <div class="icon-slider-area ptb-110" id="pricing-area">
                <div class="container">
                    <div class="row">          
                        <div class="item_all indicator-style3">
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/2.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/3.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/4.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/5.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/2.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/3.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/4.png" alt="" /></a>
                            </div>
                            <div class="col-md-12 single-item2">
                                <a href="#"><img src="img/logo-slider/5.png" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- contact area
            ============================================ -->
            <div class="map-area" id="support-area">
                <div class="contact-map">
                    <div id="hastech"></div>
                </div>
                <div class="container map-contact">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 text-center">
                            <div class="contact-from gray-bg">
                                <form id="contact-form"  method="post">
                                    <input name="name" type="text" placeholder="Nome">
                                    <input name="email" type="text" placeholder="Email">
                                    <textarea name="message" placeholder="Mensagem"></textarea>
                                    <button class="submit" type="submit">ENVIAR</button>
                                </form>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact-area">
                <div class="container">
                    <div class="row">
                        <div class="conatct-info fix">
                            <div class="col-md-4 col-sm-4 text-center">
                                <div class="single-contact-info">
                                    <div class="contact-icon">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </div>
                                    <div class="contact-text">
                                        <span>
                                            71 0000-0000
                                            <br>
                                            71 0000-0000
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <div class="single-contact-info res-xs2">
                                    <div class="contact-icon">
                                        <i class="fa fa-globe" aria-hidden="true"></i>
                                    </div>
                                    <div class="contact-text">
                                        <span>
                                            <a href="#">exemplo@gmail.com </a>
                                                <br>
                                            <a href="#">www.acbconsultoria.com.br</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 text-center">
                                <div class="single-contact-info">
                                    <div class="contact-icon">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </div>
                                    <div class="contact-text">
                                        <span>
                                            Lorem ipsum sit amet,
                                            <br>
                                            Lorem ipsum sit amet.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer area
            ============================================ -->
            <footer class="footer-area pt-100">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <div class="footer-all">
                            <div class="footer-logo logo">
                                <a href="#"><img src="" alt="LOGO"></a>
                            </div>
                            <div class="footer-icon">
                                <p>Lorem ipsum sit amet, consectetur adipiscing elit.<br> Donec et lorem nec arcu lacinia suscipit quis vitae tortor. </p>
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="footer-text">
                                <span>
                                    Copyright©
                                    <a href="#" target="_blank">A.C.B Consultoria</a>
                                    2018.Todos direitos reservados.<br> 
                                    Desenvolvido por <a href="www.temet.com.br" target="_blank">TEMET Desenvolvimento</a> 
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- start scrollUp
            ============================================ -->
            <div id="toTop">
                <i class="fa fa-chevron-up"></i>
            </div>
        </div>
        
        
		<!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
        <!-- ajax mails JS
        ============================================ -->
        <script src="js/ajax-mail.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
        <!-- google map api -->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_qDiT4MyM7IxaGPbQyLnMjVUsJck02N0"></script>
        <script>
            var myCenter=new google.maps.LatLng(30.249796, -97.754667);
            function initialize()
            {
                var mapProp = {
                    center:myCenter,
                    scrollwheel: false,
                    zoom:15,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                };
                var map=new google.maps.Map(document.getElementById("hastech"),mapProp);
                var marker=new google.maps.Marker({
                    position:myCenter,
                    animation:google.maps.Animation.BOUNCE,
                    icon:'img/map-marker.png',
                    map: map,
                });
                var styles = [
                    {
                        stylers: [
                            { hue: "#c5c5c5" },
                            { saturation: -100 }
                        ]
                    },
                ];
                map.setOptions({styles: styles});
                marker.setMap(map);
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
		<!-- main JS
		============================================ -->		
        <script src="js/main.js"></script>
    </body>
</html>
